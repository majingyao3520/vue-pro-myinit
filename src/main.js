import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 全局导入
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import './icons/index.js';

//按需导入 暂时没搞懂
// import '@/element-ui'
Vue.config.productionTip = false;
Vue.use(ElementUI);
import './router/permit.js'
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
