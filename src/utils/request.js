// 1.先安装axios npm i axios
import axios from 'axios';
import { Message } from 'element-ui';
const BASEURL = process.env.NODE_ENV == 'production' ? '' : '/devApi'
const service = axios.create({
  baseURL: BASEURL,
  timeout: 10000,
});
// onsole.log(process.env.NODE_ENV);可以打印出来在什么环境
// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  // 在请求头做些什么
  console.log('打印请求头', config.headers)
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  let data = response.data;
  if (data.resCode != 0) {
    Message.error(data.message);
    return Promise.reject(data);
  } else {
    return response;
  }

}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default service;