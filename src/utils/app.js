import Cookie from 'cookie_js'
const admin_token = 'admin_token'
export function getToken () {
  return Cookie.get(admin_token)
}
export function setToken (token) {
  return Cookie.set(admin_token, token)
}
export function removeToken (token) {
  return Cookie.remove(admin_token)
}
export function setUsername (val) {
  return Cookie.set('username', val)
}
export function removeUsername (val) {
  return Cookie.remove('username')
}