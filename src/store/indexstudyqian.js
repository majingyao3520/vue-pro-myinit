// import Vue from "vue";
// import Vuex from "vuex";
// import Cookie from 'cookie_js';
// Vue.use(Vuex);

// export default new Vuex.Store({

//   // state是存储的单一状态 是存储的基本数据
//   state: {
//     // isCollapse: JSON.parse(sessionStorage.getItem('isCollapse')) || false,
//     isCollapse: Cookie.get('isCollapse') || false,
//     count: 10,
//   },
//   //getters是store的计算属性 对state进行加工
//   getters: {
//     count: state => {
//       return state.count + 20;
//     }
//   },
//   //mutation提交更改数据，使用store.commit方法更改state的存储状态，mutations是同步函数
//   // 直接this.$store.commit.set_count来调用
//   mutations: {
//     set_iscollapse (state, value) {
//       state.isCollapse = !state.isCollapse;
//       //关闭浏览器时失效，只能存储字符串 而localStorage是手动清除
//       sessionStorage.setItem('isCollapse', JSON.stringify(state.isCollapse))
//       // Cookie.set('isCollapse', JSON.stringify(this.$store.state.isCollapse))
//     },
//     set_count (state, value) {
//       state.count = value;
//     }
//   },
//   //action主要处理异步
//   actions: {
//     // 第一个参数对应store里的东西 第二个参数对应传的数据
//     setMenuState (content, data) {
//       // content.state
//       // content.commit
//       // content.getters
//       //1.同步修改mutation里的东西 content.commit('set_count')
//       console.log('看看action里的参数第一个', content)

//     }
//   },
//   modules: {},
// });
