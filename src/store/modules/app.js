import Cookie from 'cookie_js'
const state = {
  isCollapse: JSON.parse(sessionStorage.getItem('isCollapse')) || false,
  // isCollapse: Cookie.get('isCollapse') || false,
  count: 10,
  admin_token: '',
  username: Cookie.get('username') || '',
}
const getters = {
  count: state => {
    return state.count + 20;
  }
}
const mutations = {
  set_iscollapse (state, value) {
    state.isCollapse = !state.isCollapse;
    //关闭浏览器时失效，只能存储字符串 而localStorage是手动清除
    sessionStorage.setItem('isCollapse', JSON.stringify(state.isCollapse))
    // Cookie.set('isCollapse', JSON.stringify(this.$store.state.isCollapse))
  },
  set_count (state, value) {
    state.count = value;
  },
  set_Token (state, value) {
    state.admin_token = value;
  },
  set_username (state, value) {
    state.username = value
  }
}
const actions = {
  setMenuState (content, data) {

    console.log('看看action里的参数第一个', content)


  }
}
// const app = {
//   // state是存储的单一状态 是存储的基本数据
//   state: {

//   },
//   //getters是store的计算属性 对state进行加工
//   getters: {

//   },
//   //mutation提交更改数据，使用store.commit方法更改state的存储状态，mutations是同步函数
//   // 直接this.$store.commit.set_count来调用
//   mutations: {

//   },
//   //action主要处理异步
//   actions: {

//   },
// }

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}