import service from '@/utils/request'
// 获取验证码
export function getSms (data) {
  return service.request({
    method: 'post',
    url: '/getSms/',
    data: data
  })
}

//注册按钮
export function getRegister (data) {
  return service.request({
    method: 'post',
    url: '/register/',
    data
  })
}

//登录按钮
export function getLogin (data) {
  return service.request({
    method: 'post',
    url: '/login/',
    data
  })
}