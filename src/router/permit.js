//路由导航守卫
import router from './index'
import { getToken, removeToken, removeUsername } from '@/utils/app.js'
import store from '@/store/index'

const routerWhite = ['/login']
router.beforeEach((to, from, next) => {
  if (getToken()) {
    if (to.path == '/login') {
      //清除cookie里存的token跟username
      removeToken();
      removeUsername();
      //清除vuex里的token跟username
      store.commit('app/set_Token', '')
      store.commit('app/set_username', '')

    }
    next()


  } else {
    if (routerWhite.indexOf(to.path) !== -1) {
      next();
    } else {
      next('/login')
    }
  }
})