import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  // {
  //   path: "/",
  //   name: "Home",
  //   component: Home,
  // },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue"),
  // },
  {
    path: "/",
    redirect: "/login",
    hidden: true,
    meta: {
      title: '主页'
    }
  },
  {
    path: "/login",
    name: "Login",
    hidden: true,
    meta: {
      title: '登录',
    },
    component: () => import("../views/Login/index.vue"),
  },
  {
    path: "/demos",
    name: "Demo",
    hidden: true,
    meta: {
      title: '模板',
      // icon: 'el-icon-s-help',

    },
    component: () => import("../demos/demoLayout/index.vue"),
  },
  {
    path: "/layout",
    name: "Layout",
    redirect: '/index',
    meta: {
      title: '控制台',
      // icon: 'el-icon-s-cooperation'
      icon: 'user'
    },
    component: () => import("../views/Layout/index.vue"),
    children: [{
      path: "/index",
      name: "Index",
      meta: {
        title: '首页',
      },
      component: () => import("../views/Console/index.vue")
    }]
  },
  {
    path: "/info",
    name: "Info",
    meta: {
      title: '信息管理',
      // icon: 'el-icon-s-grid'
      icon: 'userinfo'
    },
    component: () => import("../views/Layout/index.vue"),
    children: [{
      path: "/infoList",
      name: "InfoList",
      meta: {
        title: '信息列表',
      },
      component: () => import("../views/Info/list.vue")
    }, {
      path: "/InfoCategory",
      name: "InfoCategory",
      meta: {
        title: '信息分类'
      },
      component: () => import("../views/Info/category.vue")
    }]
  },
  {
    path: '/user',
    name: 'User',
    meta: {
      title: '用户管理',
      // icon: 'el-icon-s-check'
      icon: 'info'
    },
    component: () => import("../views/Layout/index.vue"),
    children: [{
      path: '/userList',
      name: 'UserList',
      meta: {
        title: '用户列表',
      },
      component: () => import("../views/User/list.vue")
    }]
  }

];

const router = new VueRouter({
  routes,
});
//在这里写beforeEach也可以
// router.beforeEach((to, from, next) => {
//   console.log('看看to', to);
//   console.log('看看from', from);
//   console.log('看看next', next);
//   next()
// })
export default router;
