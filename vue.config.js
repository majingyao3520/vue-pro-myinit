const path = require('path')
module.exports = {
  publicPath: process.env.NODE_ENV == 'production' ? '' : '/', // 根域上下文目录
  outputDir: process.env.NODE_ENV == 'production' ? 'dist' : 'devdist', // 构建输出目录
  assetsDir: 'assets', // 静态资源目录 (js, css, img, fonts)
  lintOnSave: false, // 是否开启eslint保存检测，有效值：ture | false | 'error'
  // runtimeCompiler: true, // 运行时版本是否需要编译
  // transpileDependencies: [], // 默认babel-loader忽略mode_modules，这里可增加例外的依赖包名
  productionSourceMap: false, // 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度
  //webpack配置节点
  // configureWebpack: {
  //   // 路径配置
  //   resolve: {
  //     extensions: ['.js', '.vue', '.json'],
  //     // 别名配置
  //     alias: {
  //       // @ is an alias to /src
  //       '@': resolve('src'),
  //     }
  //   }
  // },
  configureWebpack: {
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.js'
      }
    },
  },
  chainWebpack: config => { // webpack链接API，用于生成和修改webapck配置，https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')//要npm i svg-sprite-loader -S
      .options({
        symbolId: 'icon-[name]',
        include: ["./src/icons"]
      })
  },
  parallel: require('os').cpus().length > 1, // 构建时开启多进程处理babel编译
  pluginOptions: { // 第三方插件配置
  },
  pwa: { // 单页插件相关配置 https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
  },
  // css相关配置
  // css: {
  //   // 是否使用css分离插件 ExtractTextPlugin 生产环境下是true,开发环境下是false
  //   extract: true,
  //   // 开启 CSS source maps?
  //   sourceMap: false,
  //   // css预设器配置项
  //   loaderOptions: {
  //     sass: {
  //       data: `@import "./src/styles/main.scss";`
  //     }
  //   },
  //   // 启用 CSS modules for all css / pre-processor files.
  //   modules: false
  // },
  devServer: {
    // open: true,
    // host: 'localhost',
    // port: 8080,
    // https: false,
    // hotOnly: false,
    proxy: { // 配置跨域
      '/devApi': {
        target: 'http://www.web-jshtml.cn/productapi/token',	//接口域名
        ws: true,	//是否代理websockets
        changOrigin: true,	//是否跨域
        pathRewrite: {		//重置路径
          '^/devApi': ''
        }
      }
    },
    before: app => { }
  }
}
